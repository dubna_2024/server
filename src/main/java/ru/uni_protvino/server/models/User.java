package ru.uni_protvino.server.models;

import java.time.LocalDate;

import lombok.Data;

@Data
public class User {

    public enum Gender {
        MALE, FEMALE
    }

    private Long id;
    private String firstName;
    private String middleName;
    private String lastName;
    private String login;
    private String email;
    private String password;
    private LocalDate birthDate;
    private Gender gender;
}
