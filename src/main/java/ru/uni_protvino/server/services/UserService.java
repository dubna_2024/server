package ru.uni_protvino.server.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import net.datafaker.Faker;
import ru.uni_protvino.server.models.User;

@Service
public class UserService {

    private Faker faker = new Faker(new Locale( "ru", "RU" ));
    
    public List<User> getAllUsers() {
        List<User> userList = new ArrayList<>();
        for ( int i = 1; i <= 10; i++ ) {
            userList.add( createMockUser( i ));
        }
        return userList;
    }

    public User getById(Long id) {
        if ( id < 0 )
            throw new IllegalArgumentException( "ID cannot have a negative value!" );
        else if ( id > 10 ) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        else 
            return createMockUser(id);
    }

    private User createMockUser( long id ) {
        User u = new User();
        u.setId( id );
        u.setLogin( faker.internet().username() );
        u.setEmail( faker.internet().emailAddress());
        u.setPassword( faker.random().hex( 16 ));
        
        User.Gender g = faker.random().nextBoolean() ? User.Gender.MALE : User.Gender.FEMALE;
        u.setGender( g );
        
        if ( g == User.Gender.MALE ) 
            u.setFirstName( faker.name().malefirstName() );
        else 
            u.setFirstName( faker.name().femaleFirstName() );
        
        u.setLastName( faker.name().lastName() );
        u.setMiddleName( null );
        u.setBirthDate(faker.date().birthdayLocalDate( 18, 81 ));
        
        return u;
    }
}
