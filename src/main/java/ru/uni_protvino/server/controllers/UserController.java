package ru.uni_protvino.server.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.extern.slf4j.Slf4j;
import ru.uni_protvino.server.models.User;
import ru.uni_protvino.server.services.UserService;


@Slf4j
@RestController
@RequestMapping("api/users")
@CrossOrigin
public class UserController {

    @Autowired
    UserService userService;

    @Operation(summary = "Получение списка пользователей")
    @GetMapping("/all")
    public ResponseEntity<List<User>> getAll() {    
        List<User> list = userService.getAllUsers();
        return ResponseEntity.ok( list );
    }
    

    @Operation(summary = "Получение одной записи пользователя по Id")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<User> getOne(
        @Parameter(name = "id", description = "Идентификатор записи")
        @PathVariable 
        Long id )  
    {    
        try {
            User user = userService.getById(id);
            return ResponseEntity.ok( user );
        }
        catch(ResponseStatusException e ) {
            log.error( e.getMessage(), e);
            return ResponseEntity.status( e.getStatusCode() ).build();
        }
        catch(Exception e) {
            log.error( e.getMessage(), e);
            return ResponseEntity.status( HttpStatus.INTERNAL_SERVER_ERROR ).build();
        }
    }
}