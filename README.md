# Demo server uni-protvino 2024

## Подготовка 

Проект написан на ЯП Java версии 21 с использованием системы сборки [Gradle Build Tool](https://gradle.org/)

Для того чтобы установить последнюю версию Java, перейдите по ссылке https://jdk.java.net/21/

Скопируйте ссылку на скачивание и выполните следующие команды: 

```shell
curl -O https://download.java.net/java/GA/jdk21.0.1/415e3f918a1f4062a0074a2794853d0d/12/GPL/openjdk-21.0.1_windows-x64_bin.zip
unzip openjdk-21.0.1_windows-x64_bin.zip
export JAVA_HOME=$(readlink.exe -f jdk-21.0.1/)
```

Более старые версии можно найти по адресу https://jdk.java.net/archive/

## Структура проекта

```shell
├── .git             # git репозиторий
├── build/           # Папка, в которую помещаются собранные .class файлы
├── .gradle/         # Скрытая папка, в котоую скачивается приложение gradle
├── gradle/          # Обёртка gradle приложения, которая делает установку нужной версии gradle
├── src              # Исходный код приложения
├── .gitignore       # Файл, в котором перечисляются файлы и папки для их игнорирования git
├── build.gradle     # Конфигурационый файл gradle описывающий правила сборки проекта
├── gradlew          # Исполняемый файл обёртки gradle (для Linux)
├── gradlew.bat      # Исполняемый файл обёртки gradle (для Windows)
├── README.md        # Файл с описанием проекта в формате Markdown
└── settings.gradle  # Конфигурационый файл gradle с параметрами
```

### Структура папки с исходным кодом

```shell
src/
├── main
│    ├── java              # Java код
│    │    └── ru
│    │        └── uni_protvino
│    │            └── server
│    │                ├── controllers   # Классы контролёры доступа к API 
│    │                ├── models        # Классы описывающие модели приложения
│    │                └── services      # Классы реализующие бизнес-логику
│    └── resources          # Ресурсы проекта
│        ├── static         
│        └── templates
└── test                   # Тесты 
    └── java
        └── ru
            └── uni_protvino
                └── server
```

## Сборка 

```shell
gradlew.bat classes      # Выполнит компиляцию java файлов в байт код (.class в папку build)
gradlew.bat clean        # Очистит папку build
gradlew.bat jar          # Выполнит упаковку .class файлов в jar (Java ARchive)
gradlew.bat test         # Выполнит тесты 
gradlew.bat build        # Выполнит сборку проекта в jar-файл и запустит тесты
gradlew.bat bootRun      # Запустит приложение
gradlew.bat tasks        # Покажет список команд gradle
```

## Создание делегата для диаграммы BPMN

### Подготовка 

Для взаимодействия нашего spring-boot приложения с движком Camunda, нам потребуется подключить специальную библиотеку,
которая будет выполнять роль клиента для соединения с сервером Camunda и выполнять внешние задачи (External Tasks).
 
Для этого добавьте в файл `build.gradle`, в секцию `dependencies`,  следующую строку: 

```groovy
implementation 'org.camunda.bpm.springboot:camunda-bpm-spring-boot-starter-external-task-client:7.20.0'
```

Это нигде не документировано, но чтобы потом долго не искать причину странных ошибок добавьте библиотеку `jaxb-api`, 
которая требуется для работы `external-task-client`:
```groovy
implementation 'javax.xml.bind:jaxb-api:2.3.1'
```

Клиент может подписаться на одно или несколько топиков (типов задач), определенных в вашей модели процесса BPMN. 
Когда некий шаг на диаграмме ожидает выполнения внешней задачи, клиент может получить этот запрос и начать выполнение бизнес-логики. 
Например, проверка наличия необходимой информации по клиенту в БД, и в случае успеха задача может быть помечена как выполненная, 
и движение процесса по BPMN продолжится.

### Запуск сервера Camunda и загрузка BPMN диаграммы

#### Сервер Camunda

Для запуска сервера Camunda нам нужно его скачать. Сделать это можно по ссылке: https://camunda.com/download/platform-7/

Сервер Camunda написан на Java и является кроссплатформенным, но для его запуска также требуется Java версии 17 и выше. 
Если Java нужной версии установленна в системе, то достаточно вызвать файл `start.bat` для Windows, или `start.sh` для Linux. 

Если нужно запустить сервер с Java из локальной папки, то можно отредактировать файл `start.bat` прописав в нём 
для переменной `JAVA_HOME` путь к этой папке и добавив значение `%JAVA_HOME%\bin` в переменную `PATH`. 
Следующим образом:

```batch
set JAVA_HOME=F:\JAVA
set PATH=%JAVA_HOME%\bin;%PATH%
```

После запуска скрипта должен автоматически открыться браузер со страницей Welcome Camunda. 
На всякий случай, ниже прямые ссылки на необходимые нам приложения:   

1. Task list: http://localhost:8080/camunda/app/tasklist  - для старта процессов и управлением пользовательскими задачами.
2. Cockpit: http://localhost:8080/camunda/app/cockpit/  - для мониторинга прохождения БП.

#### Camunda Modeler

Camunda Modeler не кроссплатформенный, поэтому запускается проще. Нужно скачать нужную версию программы для своей ОС, 
перейдя по этой ссылке:
https://camunda.com/download/modeler/

Далее распаковать скаченный архив и запустить lunch файл, соответствующий вашей ОС. 

Откройте bpmn диаграмму (File -> Open File), готовый файл есть в проекте, 
в папке `src/main/resources/License_granting.bpmn`. 
И, при запущенном сервере Camunda, выполните "Deploy current diagram" (cм. рисунок ниже):

![Загрузка диаграммы на сервер](readme_image_deploy_bpmn.png)

Если всё прошло успешно, то можно перейти в [Camunda Cockpit](http://localhost:8080/camunda/app/cockpit/default/#/processes)
и увидеть загруженную диаграмму: 

![Загруженная диаграмма](readme_image_deployed_diagram.png)

Теперь можно приступать к написанию обработчика.

### Пишем свой первый обработчик внешних задач


Создадим в проекте новый класс, реализующий интерфейс `ExternalTaskHandler` из подключенной нами ранее библиотеки. 
Данный интерфейс потребует добавить в класс реализацию метода `execute(ExternalTask ..., ExternalTaskService ...)`:

```java
package ru.uni_protvino.server.camunda;

import org.camunda.bpm.client.task.ExternalTask;
import org.camunda.bpm.client.task.ExternalTaskHandler;
import org.camunda.bpm.client.task.ExternalTaskService;
import org.camunda.bpm.client.spring.annotation.ExternalTaskSubscription;
import org.springframework.stereotype.Component;

@Component
@ExternalTaskSubscription(value = "LicenseGrantingTopic", localVariables = true)
public class LicenseGrantingExternalTask implements ExternalTaskHandler {

    @Override
    public void execute(ExternalTask externalTask, ExternalTaskService externalTaskService) {
    }   
}
```

#### Подписка на задачи 

Для того чтобы наш класс начал выполнять какие-то внешние задачи от Camunda, нам необходимо его подписать на топик, 
через который он будет получить задания. Для этого над классом добавим такую аннотацию: 

```java
@ExternalTaskSubscription(value = "dummy", localVariables = true)
```

Здесь `value` - это наименование топика, а `localVariables` - флаг, говорящий, что мы хотим видеть в нашем делегате
только те переменные, которые ему предназначены (определяются в диаграмме).

Так же нам необходимо добавить аннотацию `@Component`, чтобы сообщить spring-boot фреймворк, что этот класс нужно 
создавать при старте приложения. 

#### Обработка задачи

```java
package ru.uni_protvino.server.camunda;

import org.camunda.bpm.client.task.ExternalTask;
import org.camunda.bpm.client.task.ExternalTaskHandler;
import org.camunda.bpm.client.task.ExternalTaskService;
import org.camunda.bpm.client.spring.annotation.ExternalTaskSubscription;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import ru.uni_protvino.server.models.User;
import ru.uni_protvino.server.services.UserService;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
@ExternalTaskSubscription(value = "LicenseGrantingTopic", localVariables = true)
public class LicenseGrantingExternalTask implements ExternalTaskHandler {

    @Autowired
    UserService userService;

    @Override
    public void execute(ExternalTask externalTask, ExternalTaskService externalTaskService) 
    {
		try 
		{
            Long userId = externalTask.getVariable("userId");
            String licenseNumber = externalTask.getVariable("licenseNumber");
            User user = userService.getById( userId );

            log.info( "User '{} {}' granted a license with number '{}'",
                        user.getLastName(), user.getFirstName(), licenseNumber );

            // Установим флаг, что лицензия выдана 
            Map<String, Object> varsToPut = new HashMap<>();
            varsToPut.put("success", true );

            // Complete task and send to camunda
            externalTaskService.complete(externalTask, varsToPut);
            log.info("The External Task={} with topic={} has been completed!",
                    externalTask.getId(),
                    externalTask.getTopicName());
        }
        catch (Exception e) {
            //Обработка ошибок (Вызов ErrorHandler (по коду))
            externalTaskService.handleBpmnError(externalTask, "ERROR");
        }
    }
}
```

### Сборка, конфигурация приложения и запуск

Выполним компиляцию проекта.
```bash
./gradlew classes
```
Компиляция должна успешно завершиться. Но если мы попытаемся запустить приложение прямо сейчас, то оно непременно завершится с ошибкой. 
Дело в том, что теперь при старте оно пытается выполнить подписку на External Task, но мы не сообщили приложению,
где располагается работающий сервер Camunda, к которому мы собираемся подключиться. Чтобы это сделать, 
в конфигурационном файле приложения `src/main/resources/application.properties` добавьте следующую строку: 

```properties
camunda.bpm.client.base-url=http://localhost:8080/engine-rest
```

Здесь, наблюдательный читатель, может заметить что в адресе указан порт 8080 - точно такой же как и порт по умолчанию 
для нашего сервера, а это конфликт! Как минимум одно из приложений не будет стартовать, или подключение будет работать 
некорректно. Чтобы этого избежать, укажите в конфигурации другой порт для запуска сервера, например:

```properties
server.port=8090
```

Также добавьте настройки для логирования отладочной информации, чтобы можно было наблюдать происходящее на сервере:

```properties
logging.level.root=INFO
logging.level.ru.uni_protvino.server=DEBUG
logging.level.org.camunda.bpm.client.spring=DEBUG
```

#### Запуск сервера и выполнение БП 

Запускаем сервер
```bash
./gradlew bootRun
```

Идём в [Camunda Task List](http://localhost:8080/camunda/app/tasklist) и нажимаем кнопку "Start process" (1),
в открывшемся окне выбираем наш БП (2):

![Start process](readme_image_start_process.png)

Вводим "Business Key", сейчас это может быть любое слово или число, пока не принципиально, говорим "Start". 
Процесс запущен - это можно увидеть на странице Cockpit. Мы должны встать на первом User Task с названием 
"Введите ID пользователя" и теперь Camunda ждёт действий пользователя. Эти действия можно проделать, вернувшись и/или 
обновив страницу Task List: 

![User Task](readme_image_user_task.png)

Введите какие-нибудь значения и завершите задачу (кнопка "Complete"). Теперь в дело вступает External Task, 
на который должен быть подписан наш сервер. Дальнейшие шаги будут зависеть от реализации вашего делегата.  

По умолчанию, клиент (встроенная библиотека в наш сервер) проверяет наличие задач по подписке раз в какой-то период,  
чтобы задачи исполнялись немедленно - добавьте в конфигурационный файл приложения ещё такой параметр: 
```properties
camunda.bpm.client.disable-backoff-strategy=true
```

## Программный запуск БП

Для того чтобы запустить БП из нашего web-приложения, нам потребуется написать контроллер на нашем сервере, который, 
в свою очередь, будет вызывать сервер Camunda с нужными ключами, тем самым, стартовать БП и сохранять 
в БД ключи (идентификатор процесса и пользователя) для дальнейшей обработки этого БП. 

Для начала создадим spring service, который будет отвечать за обращение к серверу Camunda:

```java
package ru.uni_protvino.server.services;

import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CamundaService {
}
```

Добавим в него метод, который будет выполнять REST запросы, он будет иметь довольно общий вид, без конкретизации, 
что именно мы отправляем, куда и зачем: 

```java
    // Позволяет формировать строку в формате json из java классов и наоборот
    private final ObjectMapper mapper = new ObjectMapper();
    // Класс, который позволяет формировать HTTP запросы
    private final RestTemplate restTemplate = new RestTemplate();
    
    public JsonNode sendRequest(HttpMethod method, String url, Object body) throws Exception
    {
        log.debug("Send request to {}", url);
        log.debug("Request body: {}", body);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> requestEntity = new HttpEntity<>(body, headers);

        RestTemplate restTemplate = new RestTemplate();
        String response = restTemplate.exchange(
                url,
                method,
                requestEntity,
                String.class
            ).getBody();

        log.debug("Responce: {}", response);

        JsonNode json = mapper.readTree( response );
        return json;
    }
```

Добавим ещё два вспомогательных метода, которые упростят нам работу с сервисом при обращении к нему из контроллера:

```java
    // URL адрес к серверу Camunda из файла application.properties
    @Value("${camunda.bpm.client.base-url}")
    String CAMUNDA_BASE_URL;

    private static final String START = "start";
    private static final String PROCESS_DEFINITION_KEY = "process-definition/key";
    private static final String BUSINESS_KEY = "businessKey";


    public String startBusinessProcess(String definition, Object businessKey) throws Exception
    {
        log.debug("Try to start new business process '{}' with businessKey '{}'", definition, businessKey);
        // Формируем URL для отправки запроса 
        String url = createUrl(PROCESS_DEFINITION_KEY, definition, START);
        // Формируем тело запроса в виде объекта, готового к сериализации в json
        ObjectNode requestBody = mapper.createObjectNode().put(BUSINESS_KEY, businessKey.toString());
        // Отправляем запрос
        JsonNode json = sendRequest(HttpMethod.POST, url, requestBody);
        // Извлекаем из полученного ответа идентификатор созданного процесса
        String processInstanceId = json.get( "id" ).textValue();
        log.debug("New process instance ID='{}'", processInstanceId);
        return processInstanceId;
    }

    /** Вспомогательный метод для формирования строку URL */
    private String createUrl(String... paths) {
        return CAMUNDA_BASE_URL + String.join("/", paths);
    }
```

Список импортов, в помощь: 
```java
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.beans.factory.annotation.Value;

import lombok.extern.slf4j.Slf4j;
```


Теперь создайте контроллер, по образу и подобию `UserController`. 


Теперь ещё раз компилируем и запускаем сервер. После чего открываем в браузере swagger по адресу:

http://localhost:8090/swagger-ui/index.html#/

Здесь указан порт 8090 - это, с учётом того, что мы указали именно такой порт в `application.properties` (см. выше).